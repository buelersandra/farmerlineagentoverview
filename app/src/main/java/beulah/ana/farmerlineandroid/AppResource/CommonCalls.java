package beulah.ana.farmerlineandroid.AppResource;


import android.content.Context;
import android.support.v7.app.ActionBar;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import beulah.ana.farmerlineandroid.R;

public class CommonCalls {


    public static void setMapLocation(GoogleMap googleMap, LatLng latLng){
        if(googleMap!=null){
            googleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_yellow)));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,10));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(20), 2000, null);
        }

    }

    public static void setBar(ActionBar actionBar, Context context){
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME |ActionBar.DISPLAY_SHOW_TITLE|ActionBar.DISPLAY_USE_LOGO);
        actionBar.setIcon(R.drawable.ic_bar);
        actionBar.setTitle("  "+context.getResources().getString(R.string.app_name));
    }
}
