package beulah.ana.farmerlineandroid.AppResource;

import android.content.Context;
import android.content.SharedPreferences;

import beulah.ana.farmerlineandroid.Model.MyPlace;

public class LocationManager {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Context context;

    private static final String PREF_NAME ="AgentLocation";

    private static final String PLACE_ID = "PLACE_ID";
    private static final String PLACE_NAME = "PLACE_NAME";
    private static final String PLACE_LAT = "PLACE_LAT";
    private static final String PLACE_LNG = "PLACE_LNG";
    private static final String PLACE_SAVED = "PLACE_SAVED";


    public LocationManager(Context context){
        this.context=context;
        this.sharedPreferences = context.getSharedPreferences(PREF_NAME,context.MODE_PRIVATE);
        this.editor = sharedPreferences.edit();
    }

    public void setLocation(MyPlace place){
        editor.putString(PLACE_ID,place.getId());
        editor.putString(PLACE_NAME,place.getName());
        editor.putString(PLACE_LAT,place.getLat().toString());
        editor.putString(PLACE_LNG,place.getLng().toString());
        editor.putBoolean(PLACE_SAVED,true);
        editor.commit();

    }

    public MyPlace getLocation(){
        return new MyPlace(
                sharedPreferences.getString(PLACE_ID,null),
                sharedPreferences.getString(PLACE_NAME,null),
                Double.valueOf(sharedPreferences.getString(PLACE_LAT,"0")),
                Double.valueOf(sharedPreferences.getString(PLACE_LNG,"0"))
        );
    }

    public boolean isLocationSet(){
        return sharedPreferences.getBoolean(PLACE_SAVED,false);
    }
}
