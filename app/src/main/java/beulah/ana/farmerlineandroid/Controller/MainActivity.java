package beulah.ana.farmerlineandroid.Controller;


import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import beulah.ana.farmerlineandroid.AppResource.CommonCalls;
import beulah.ana.farmerlineandroid.AppResource.LocationManager;
import beulah.ana.farmerlineandroid.Model.MyPlace;
import beulah.ana.farmerlineandroid.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    LocationManager locationManager;
    GoogleMap gMap;
    String TAG=MainActivity.class.getSimpleName();
    View maplayout,addlocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        locationManager=new LocationManager(getApplicationContext());
        initView();

    }


    void initView(){
        ActionBar actionBar=getSupportActionBar();
        if(actionBar!=null){
            CommonCalls.setBar(actionBar,getApplicationContext());
        }

        addlocation=findViewById(R.id.addlocation);
        addlocation.setOnClickListener(this);
        addlocation.setVisibility(View.GONE);

        maplayout=findViewById(R.id.maplayout);
        maplayout.setVisibility(View.GONE);
        maplayout.setOnClickListener(this);

        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapfragment);

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                gMap=googleMap;
                checkSavedLocation();

            }
        });

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                initBar();
            }
        };
        AsyncTask.execute(runnable);



    }

    void checkSavedLocation(){
        if(gMap==null)return;

        if(locationManager.isLocationSet()) {
            MyPlace place=locationManager.getLocation();
            maplayout.setVisibility(View.VISIBLE);
            LatLng savedlocation = new LatLng(place.getLat(), place.getLng());
            CommonCalls.setMapLocation(gMap, savedlocation);

        }else{
            addlocation.setVisibility(View.VISIBLE);
        }
    }

    void initBar(){
        final BarChart chart = findViewById(R.id.barchart);

        List<BarEntry> entries = new ArrayList<>();
        entries.add(new BarEntry(2f, 28000f));
        entries.add(new BarEntry(4f, 44000f));
        entries.add(new BarEntry(6f, 13000f));



        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(10f);
        xAxis.setTextColor(Color.BLACK);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(false);

        final String[] quarters = new String[] { "Input Instant Sales", "Input Sales on Credit", "Input Sales of Saving" };

        IAxisValueFormatter formatter = new IAxisValueFormatter() {

            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                String msg="";
                switch ((int)value){
                    case 2:
                        msg=quarters[0];break;
                    case 4:
                        msg=quarters[1];break;
                    case 6:
                        msg=quarters[2];break;
                }
                return msg;
            }


        };


        xAxis.setGranularity(1f); // minimum axis-step (interval) is 1
        xAxis.setValueFormatter(formatter);


        YAxis yAxis = chart.getAxisLeft();
        yAxis.setTextSize(12f); // set the text size
        yAxis.setAxisMinimum(0f); // start at zero
        yAxis.setAxisMaximum(50000f); // the axis maximum is 100
        yAxis.setTextColor(Color.BLACK);
        yAxis.setGranularity(1f); // interval 1
        yAxis.setCenterAxisLabels(false);




        BarDataSet bardataset = new BarDataSet(entries, "");

        final BarData data = new BarData( bardataset);
        bardataset.setColor(getResources().getColor(R.color.barchart));
        chart.setData(data);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                chart.animateY(5000);

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        checkSavedLocation();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.addlocation:
            case R.id.maplayout:
                startActivity(new Intent(MainActivity.this,MapActivity.class));
                break;
        }
    }
}
