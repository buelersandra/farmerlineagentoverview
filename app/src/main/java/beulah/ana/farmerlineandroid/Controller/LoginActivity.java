package beulah.ana.farmerlineandroid.Controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.facebook.internal.CallbackManagerImpl;
import com.facebook.login.widget.LoginButton;

import java.util.Arrays;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;

import beulah.ana.farmerlineandroid.R;

public class LoginActivity extends AppCompatActivity {

    private LoginButton fblogin;
    private CallbackManager fbcallbackManager;
    private GoogleSignInClient mGoogleSignInClient;
    private final int GOOGLE_SIGNIN=002;
    private String TAG=LoginActivity.class.getSimpleName();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


        initView();

    }

    void initView(){
        //FACEBOOK
        fblogin = (LoginButton) findViewById(R.id.fblogin_button);
        fblogin.setReadPermissions(Arrays.asList("email"));
        fbcallbackManager=CallbackManager.Factory.create();
        fblogin.registerCallback(fbcallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Toast.makeText(getApplicationContext(),"FB - Welcome "+loginResult.getAccessToken().getUserId(),Toast.LENGTH_LONG).show();
                 gotoMain();
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

        //GOOGLE
        SignInButton signInButton = findViewById(R.id.google_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, GOOGLE_SIGNIN);
            }
        });

        //LINKEDIN
        ImageButton linkedin_button=findViewById(R.id.linkedin_button);
        linkedin_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleLinkedinLogin();
            }
        });

    }

    private void gotoMain(){
        startActivity(new Intent(getApplicationContext(),MainActivity.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == GOOGLE_SIGNIN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleGoogleSignInResult(task);
        }else if(requestCode == CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode()){
            fbcallbackManager.onActivityResult(requestCode, resultCode, data);
        }else{
            LISessionManager.getInstance(getApplicationContext()).onActivityResult(this, requestCode, resultCode, data);

        }


    }

    private void handleGoogleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            Toast.makeText(getApplicationContext(),"GG - Welcome "+account.getEmail(),Toast.LENGTH_LONG).show();
            gotoMain();
        } catch (ApiException e) {
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());

        }
    }

    private void handleLinkedinLogin(){
        LISessionManager.getInstance(getApplicationContext()).init(this, buildScope()
                , new AuthListener() {
                    @Override
                    public void onAuthSuccess() {
                        Toast.makeText(getApplicationContext(), "LI - Welcome", Toast.LENGTH_SHORT).show();
                        gotoMain();
                    }

                    @Override
                    public void onAuthError(LIAuthError error) {
                        // Handle authentication errors
                        Log.e(TAG, "Auth Error :" + error.toString());
                        Toast.makeText(getApplicationContext(), "Failed to authenticate with LinkedIn. Please try again.", Toast.LENGTH_SHORT).show();
                    }
                }, true);
    }


    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS);
    }




    @Override
    protected void onStart() {
        super.onStart();
        //if signed in
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);

    }
}
