package beulah.ana.farmerlineandroid.Controller;


import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import beulah.ana.farmerlineandroid.AppResource.CommonCalls;
import beulah.ana.farmerlineandroid.AppResource.LocationManager;
import beulah.ana.farmerlineandroid.AppResource.PlaceAdapter;
import beulah.ana.farmerlineandroid.Model.MyPlace;
import beulah.ana.farmerlineandroid.R;

public class MapActivity extends AppCompatActivity {

    private MapView mapview;
    private  final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";
    private String TAG=MainActivity.class.getSimpleName();
    private AutoCompleteTextView searchPlacetxt;
    private Bundle mapViewBundle;
    private GoogleApiClient googleApiClient;
    private GoogleMap gMap;
    private LatLng defaultlatlng = new LatLng(7.946527, -1.023194);
    private String[] locationPerms=new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

    private FusedLocationProviderClient fusedLocationProviderClient;
    private MyPlace agentPlace;
    private Location lastlocation;
    private TextView textview;
    private Dialog dialog;
    private LatLng selectedLocation;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        initView();
    }

    void initView(){
        ActionBar actionBar=getSupportActionBar();
        if(actionBar!=null){
            CommonCalls.setBar(actionBar,getApplicationContext());
        }
        searchPlacetxt = findViewById(R.id.search_place);

        PlaceAdapter adapter =  new PlaceAdapter(this);
        searchPlacetxt.setAdapter(adapter);
        searchPlacetxt.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
            getGooglePlace(((MyPlace)adapterView.getItemAtPosition(position)));

            }
        });

        mapview=findViewById(R.id.map_view);
        mapview.onCreate(mapViewBundle);


        mapview.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                gMap=googleMap;
                checkLocationPermission();


            }
        });

        findViewById(R.id.savelocationbtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(agentPlace!=null){
                    showNotificationDialog();
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            LocationManager locationManager=new LocationManager(getApplicationContext());
                            locationManager.setLocation(agentPlace);
                            textview.setText(getString(R.string.done_saving));
                            dialog.setCancelable(true);
                        }
                    }, 2000);
                    return;
                }
                Toast.makeText(getApplicationContext(),"Select Location",Toast.LENGTH_LONG).show();

            }
        });

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, 0, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Log.e(TAG,"googleApiClient connection failed");
                    }
                })
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        Log.e(TAG,"googleApiClient connected");
                        checkLocationPermission();

                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        Log.e(TAG,"googleApiClient connection suspended");
                    }
                })
                .build();
    }


    void showNotificationDialog(){
        dialog = new Dialog(MapActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup);
        dialog.setCancelable(false);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        wlp.gravity = Gravity.TOP;
        window.setAttributes(wlp);
        dialog.show();

        textview=dialog.findViewById(R.id.textview);
        textview.setText(getString(R.string.saving));
    }


    void getGooglePlace(final MyPlace selectedPlace){
        Places.GeoDataApi.getPlaceById(googleApiClient, selectedPlace.getId())
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess()) {
                            final Place myPlace = places.get(0);
                            selectedLocation = myPlace.getLatLng();
                            CommonCalls.setMapLocation(gMap, selectedLocation);
                            selectedPlace.setLat(selectedLocation.latitude);
                            selectedPlace.setLng(selectedLocation.longitude);
                            agentPlace=selectedPlace;


                        }
                        places.release();
                    }
                });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }

        mapview.onSaveInstanceState(mapViewBundle);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            checkLocationPermission();
        } else {
            if(gMap!=null) gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(defaultlatlng,15));
        }

    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission
                (this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED  && ContextCompat.checkSelfPermission
                (this, Manifest.permission.ACCESS_COARSE_LOCATION)!=PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, locationPerms, 01);
        } else if (gMap != null) {
            gMap.setMyLocationEnabled(true);
            fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location currLocation) {
                    lastlocation=currLocation;
                    if (currLocation != null ) {
                        LatLng loc = new LatLng(currLocation.getLatitude(), currLocation.getLongitude());
                        gMap.clear();
                        gMap.addMarker(new MarkerOptions().position(loc).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc,15));
                    }else if(selectedLocation!=null){
                        CommonCalls.setMapLocation(gMap, selectedLocation);
                    }else{
                        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(defaultlatlng,10));
                    }
                }
            });
        }
    }

    @Override
    protected void onStart() {
        mapview.onStart();
        super.onStart();

    }

    @Override
    protected void onStop() {
        mapview.onStop();
        super.onStop();
    }


    @Override
    protected void onResume() {
        mapview.onResume();
        if(selectedLocation!=null)CommonCalls.setMapLocation(gMap, selectedLocation);
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        mapview.onDestroy();
        super.onDestroy();
    }


    @Override
    public void onLowMemory() {
        mapview.onLowMemory();
        super.onLowMemory();
    }
}
